AWS Application Load Balancer Terraform module
=====================================

[![Amol Ovhal][amol_avatar]][amol_ovhal]

[Amol Ovhal][amol_ovhal] 

  [amol_ovhal]: http://www.portfolio.amolovhal.com/
  [amol_avatar]: https://gitlab.com/uploads/-/system/user/avatar/10044525/avatar.png

Terraform module which creates Application Load Balancer on AWS.


Terraform versions
------------------

Terraform 0.12.

Usage
------

```hcl
provider "aws" {
  region = "ap-south-1"
}

module "alb" {
  source                     = "git::https://gitlab.com/tf-modules4/alb.git"
  alb_name                   = "test"
  internal                   = "false"
  security_groups_id         = ["sg-018d9b6102b5e9777"]
  subnets_id                 = ["subnet-01ca74677a1b41a25","subnet-08ededf4717b2ccfe"]
  enable_deletion_protection = "false"
  enable_logging             = "false"
  logs_bucket                = ""
  alb_certificate_arn        = ""

}

```

```
output "alb_dns_name" {
  description = "DNS of ALB"
  value = aws_lb.alb.dns_name
}

output "alb_arn" {
  description = "ARN of alb"
  value = aws_lb.alb.arn
}

output "alb_http_listener_arn" {
  description = "ARN of alb http listener"
  value = aws_alb_listener.alb_http_listener.arn
}

output "alb_https_listener_arn" {
  description = "ARN of alb https listener"
  value = aws_alb_listener.alb_https_listener.arn
}
```
Tags
----
* Tags are assigned to resources with name variable as prefix.
* Additial tags can be assigned by tags variables as defined above.

Inputs
------
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| alb_name | The string for name of the application load balancer | `string` | `"false"` | yes |
| internal | You can define the internal load balancer | `string` | `"false"` | yes |
| security_groups_id | ID's of the security group that you have to add to alb while creating an alb | `string` | `"false"` | yes |
| subnets_id | Define subnets id  | `string` | `"false"` | yes |
| enable_deletion_protection |define deletion protection | `string` | `"false"` | yes |
| enable_logging |define alb logs enabled | `list` | `"true"` | yes |
| logs_bucket |bucket for alb logs | `string` | `"false"` | yes |
| alb_certificate_arn |https certificate arn | `string` | `"false"` | yes |


Output
------
| Name | Description |
|------|-------------|
| alb_dns_name | DNS of ALB |
| alb_arn | ARN of alb |
| alb_http_listener_arn | ARN of alb http listener |
| alb_https_listener_arn | ARN of alb https listener |

## Related Projects

Check out these related projects.

- [security_group](https://gitlab.com/tf-modules4/security-group) - Terraform module for creating dynamic Security group.

- [subnets](https://gitlab.com/tf-modules4/subnet) - Terraform module for creating subnets.

### Contributor :

[![Amol Ovhal][amol_avatar]][amol_homepage]<br/>[Amol Ovhal][amol_homepage] 

  [amol_homepage]: https://gitlab.com/amol.ovhal
  [amol_avatar]: https://gitlab.com/uploads/-/system/user/avatar/10044525/avatar.png
